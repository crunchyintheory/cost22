const regions = [
    {
        id: "unitedstates",
        name: "United States",
        emoji: ["1f1fa", "1f1f8"],
        glyph: "$",
        exchangeRate: 1,
        medianIncome: 67521
    },
    {
        id: "brazil",
        name: "Brazil",
        emoji: ["1f1e7", "1f1f7"],
        glyph: "R$",
        exchangeRate: 5.16,
        medianIncome: 29328
    }
];

const consoles = [
    {
        name: "PlayStation 5",
        image: "images/ps5.jpg?webp=1",
        prices: {
            unitedstates: {
                amount: 400
            },
            brazil: {
                amount: 7097
            }
        }
    },
    {
        name: "Xbox Series X",
        image: "images/xbsx.jpg?webp=1",
        prices: {
            unitedstates: {
                amount: 500
            },
            brazil: {
                amount: 4449
            }
        }
    },
    {
        name: "Xbox Series S",
        image: "images/xbss.jpg?webp=1",
        prices: {
            unitedstates: {
                amount: 300
            },
            brazil: {
                amount: 2749
            }
        }
    },
    /*{
        name: "Nintendo Switch",
        image: "images/switch.png?webp=1",
        prices: {
            unitedstates: {
                amount: 500
            }
        }
    },*/
    {
        name: "Mid-range PC",
        image: "images/h510.jpg?webp=1",
        prices: {
            unitedstates: {
                amount: 750
            },
            brazil: {
                amount: 6718
            }
        }
    }
];

const games = [
    {
        name: "Horizon: Zero Dawn",
        prices: {
            unitedstates: {
                amount: 50
            },
            brazil: {
                amount: 200
            }
        },
        extras: [],
        steamid: 1151640
    },
    {
        name: "Destiny 2",
        prices: {
            unitedstates: {
                amount: 0
            },
            brazil: {
                amount: 0
            }
        },
        extras: [
            {
                name: "The Witch Queen",
                type: "content",
                prices: {
                    unitedstates: {
                        amount: 40
                    },
                    brazil: {
                        amount: 85
                    }
                }
            },
            {
                name: "The Witch Queen Seasons",
                type: "content",
                prices: {
                    unitedstates: {
                        amount: 30
                    },
                    brazil: {
                        amount: 64
                    }
                }
            },
            {
                name: "Beyond Light",
                type: "content",
                prices: {
                    unitedstates: {
                        amount: 30
                    },
                    brazil: {
                        amount: 90
                    }
                }
            },
            {
                name: "Shadowkeep",
                type: "content",
                prices: {
                    unitedstates: {
                        amount: 25
                    },
                    brazil: {
                        amount: 47
                    }
                }
            },
            {
                name: "Forsaken",
                type: "content",
                prices: {
                    unitedstates: {
                        amount: 20
                    },
                    brazil: {
                        amount: 38
                    }
                }
            }
        ],
        steamid: 1085660
    },
    {
        name: "Resident Evil: Village",
        prices: {
            unitedstates: {
                amount: 60
            },
            brazil: {
                amount: 180
            }
        },
        extras: [
            {
                name: "Deluxe Edition (Upgrade)",
                type: "cosmetic",
                prices: {
                    unitedstates: {
                        amount: 12
                    },
                    brazil: {
                        amount: 39
                    }
                }
            }
        ],
        steamid: 1196590
    },
    {
        name: "Dying Light 2",
        prices: {
            unitedstates: {
                amount: 60
            },
            brazil: {
                amount: 250
            }
        },
        extras: [
            {
                name: "Deluxe Edition (Upgrade)",
                type: "content",
                prices: {
                    unitedstates: {
                        amount: 20
                    },
                    brazil: {
                        amount: 50
                    }
                }
            },
            {
                name: "Ultimate Edition (Upgrade)",
                type: "content",
                prices: {
                    unitedstates: {
                        amount: 20
                    },
                    brazil: {
                        amount: 70
                    }
                }
            }
        ],
        steamid: 534380
    },
    {
        name: "Hollow Knight",
        extras: [],
        prices: {
            unitedstates: {
                amount: 15
            },
            brazil: {
                amount: 28
            }
        },
        steamid: 367520
    }
];

var currentRegion = null;

var consoleOwned = $("input[name=console_have]:checked").attr("id") == undefined ? null : ($("input[name=console_have]:checked").attr("id") == "console_yes");
var currentConsole = null;

const prices = [];

function fmtCurrency(amount, glyph) {
    return `${glyph}${amount.toLocaleString()}`;
}

function getRegionalPrice(price, forceNonRegional = false) {
    let region = currentRegion ?? regions[0];
    let amount, regional;
    let baseConverted = price["unitedstates"].amount * region.exchangeRate;
    if(Object.keys(price).includes(region.id) && !forceNonRegional) {
        amount = price[region.id].amount;
        regional = true;
    }
    else {
        amount = baseConverted;
        regional = false;
    }
    return { amount, regional, baseConverted }
}

function innerCurrency(price, forceNonRegional = false) {
    if(currentRegion == null) return "";
    let {amount, regional, baseConverted} = getRegionalPrice(price, forceNonRegional);
    let uptick = (amount == 0 || baseConverted == 0) ? 0 : amount / baseConverted * 100
    return `<span data-uptick-shown="${regional && currentRegion.id != "unitedstates"}" data-uptick-gt="${uptick > 100}" data-uptick="${uptick.toFixed(2)}%" class="${currentRegion.id === "unitedstates" ? "" : (regional ? "regional" : "nonregional")}">${fmtCurrency(amount, currentRegion.glyph)}</span>`;
}

function currency(price) {
    const id = prices.push(price) - 1;
    return `<span class="currency-value" data-price="${id}">${innerCurrency(price)}</span>`
}

function colorUpdate() {
    if($(document).scrollTop() < $(window).height()) {
        document.body.classList.add("black");
    }
    else {
        document.body.classList.remove("black");
    }
}

function doRegionPopulate() {
    regions.forEach(x => {
        const clone = $('template#regionItem').contents().clone();

        clone.find("label").attr("for", x.id).children("span").text(x.name)
        clone.find("label div img").attr("src", `https://raw.githubusercontent.com/twitter/twemoji/master/assets/svg/${x.emoji.join("-")}.svg`)
        clone.find("input").attr("value", x.id).attr("id",x.id)
    
        clone.appendTo("#regionList");
    })
}

function doConsolePopulate() {
    consoles.forEach(x => {
        const clone = $('template#consoleItem').contents().clone();

        let id = x.name.toLowerCase().replaceAll(" ", "")

        clone.find("label").attr("for", id)
        clone.find("label div img").attr("src", x.image)
        clone.find("input").attr("value", id).attr("id",id)

        clone.find(".cost").html(currency(x.prices));
        clone.find(".name").text(x.name);
    
        clone.appendTo("#consoleList");
    })
}

function doGamePopulate() {
    games.forEach(x => {
        const clone = $('template#gameItemL').contents().clone();

        let id = x.name.toLowerCase().replaceAll(" ", "")

        $(clone[3]).css("background", `linear-gradient(90deg, #777 50%, #aaa 100%), url(https://cdn.cloudflare.steamstatic.com/steam/apps/${x.steamid}/library_hero.jpg) center center/cover`);
        $(clone[1]).attr("value", `selected${id}`).attr("id",`selected${id}`);
        clone.find("input[type=checkbox]").attr("value", id).attr("id", id);

        clone.find(".price").html(currency(x.prices));
        clone.find(".name").text(x.name);
    
        clone.appendTo("#gameListL");
    });

    games.forEach(x => {
        const clone = $('template#gameItemR').contents().clone();

        let id = x.name.toLowerCase().replaceAll(" ", "")

        clone.attr("data-game", id);
        clone.find(".game").css("background", `linear-gradient(90deg, #777 50%, #aaa 100%), url(https://cdn.cloudflare.steamstatic.com/steam/apps/${x.steamid}/library_hero.jpg) center center/cover`);
        clone.find(".name").text(x.name);

        const extras = [{
            name: "Base Game",
            type: "content",
            prices: x.prices
        }].concat(x.extras);

        const types = extras.map(y => y.type).filter((y, i, z) => z.indexOf(y) == i);

        types.forEach((y, i) => {
            $(`<div class="extras-section-header mb-2 d-flex">${y}<div class="flex-grow-1"></div></div>`).appendTo(clone.find(".list-item"));
            extras.filter(z => z.type == y).forEach((z, i2) => {
                const clone2 = $('template#gameItemRExtra').contents().clone();
                const id = z.name.toLowerCase().replaceAll(" ", "");
                clone2.find("input[type=checkbox]").attr("value", id).attr("id", id);
                clone2.find(".price").html(currency(z.prices));
                clone2.find(".name").text(z.name);
    
                if(i === 0 && i2 === 0) {
                    clone2.find("input").attr("disabled", true).attr("checked", true)
                }
                else {
                    clone2.addClass("mt-3").css("cursor", "pointer");
                }
    
                clone2.appendTo(clone.find(".list-item"));
            });
        })
    
        clone.appendTo("#gameListR");
    });
}

function countryInputHandler(ev) {
    currentRegion = regions.find(x => x.name.toLowerCase().replaceAll(" ", "") == ev.currentTarget.id);
    $('.currency-value').each((i, x) => {
        if($(x).is("#gameListContainer .currency-value")) {
            if(currentConsole != null) {
                $(x).html(innerCurrency(prices[$(x).data("price")], currentConsole.name != "Mid-range PC"));
            }
        }
        else {
            $(x).html(innerCurrency(prices[$(x).data("price")]));
        }
    });
    $("#medianIncome").text(`${fmtCurrency(currentRegion.medianIncome, currentRegion.glyph)} / yr`);
    $("#do_you_console").removeClass("disabled-section");
    $("#consoleListContainer").removeClass("disabled-section");
    updateTotal();
}

function consoleHaveInputHandler(ev) {
    consoleOwned = ev.currentTarget.id == "console_yes";
    if(currentConsole != null) {
        $("#letsPlay").removeClass("disabled-section");
        $("#gameListContainer").removeClass("disabled-section");
    }
    updateTotal();
}

function consoleTypeInputHandler(ev) {
    currentConsole = consoles.find(x => x.name.toLowerCase().replaceAll(" ", "") == ev.currentTarget.id);
    $('#gameListContainer .currency-value').each((i, x) => {
        $(x).html(innerCurrency(prices[$(x).data("price")], currentConsole.name != "Mid-range PC"));
    });
    if(consoleOwned != null) {
        $("#letsPlay").removeClass("disabled-section");
        $("#gameListContainer").removeClass("disabled-section");
    }
    updateTotal();
}

function gameSelectInputHandler(ev) {
    $("#gameListR > .card").addClass("d-none").filter(`.card[data-game="${ev.currentTarget.id.substr(8)}"]`).removeClass("d-none");
}

function gameEnabledInputHandler(ev) {
    gameSelectInputHandler({ currentTarget: { id: `selected${ev.currentTarget.id}` } });
    $("#gameListL > label").attr("for", undefined).find("input:checked").each((i, x) => {
        console.log(x);
        $(x).parent().attr("for", `selected${x.id}`);
    });
    games.find(x => x.name.toLowerCase().replaceAll(" ", "") == ev.currentTarget.id).enabled = ev.currentTarget.checked;
    updateTotal();
}

function extraContentInputHandler(ev) {
    let gameid = $(ev.currentTarget).parent().parent().parent().data("game");
    let game = games.find(x => x.name.toLowerCase().replaceAll(" ", "") == gameid);
    game.extras.find(x => x.name.toLowerCase().replaceAll(" ", "") == ev.currentTarget.id).enabled = ev.currentTarget.checked;
    updateTotal();
}

function updateTotal() {
    let total = 0;
    if(currentConsole != null && !consoleOwned) {
        total += getRegionalPrice(currentConsole.prices).amount;
    }
    games.filter(x => x.enabled).forEach(x => {
        total += getRegionalPrice(x.prices, currentConsole.name != "Mid-range PC").amount;
        x.extras.filter(y => y.enabled).forEach(y => {
            total += getRegionalPrice(y.prices, currentConsole.name != "Mid-range PC").amount;
        })
    });

    $("#total").text(fmtCurrency(total, currentRegion.glyph));
    $("#total_percent").text(`${(total / currentRegion.medianIncome * 100).toFixed(2)}%`);
}

window.addEventListener('scroll', () => {
    colorUpdate();
});

$(function() {
    doRegionPopulate();
    doConsolePopulate();
    doGamePopulate();

    if(currentRegion == null) {
        $("#do_you_console").addClass("disabled-section");
        $("#consoleListContainer").addClass("disabled-section");
    }

    if(currentConsole == null || consoleOwned) {
        $("#letsPlay").addClass("disabled-section");
        $("#gameListContainer").addClass("disabled-section");
    }

    $("input[name=country]").on("input", countryInputHandler);
    $("input[name=console_have]").on("input", consoleHaveInputHandler);
    $("input[name=console_type]").on("input", consoleTypeInputHandler);
    $("input[name=game_shown]").on("input", gameSelectInputHandler);
    $("input[name=game_enabled]").on("input", gameEnabledInputHandler);
    $("input[name=extracontent]").on("input", extraContentInputHandler);
})
